# Wardrobify

Team:

* Meghan Hyde - Hats
* Juan Melendrez - Shoes


## Design
Design:

Nav: the navigation bar is consistent throughout the entire application, offering access to all of the apps sections, this choice allows users to navigate between pages quickly. It includes links to various sections: Home, Shoes, Hats, Add a shoe, and Add a hat.

Home: The home section is the landing page which includes the project name and a promotional message.

Shoes: The shoes section of the page presents an organized list of the user’s shoes in the form of a card design for every shoe. The card includes an image of the shoe, its manufacturer, name, color, and bin location. Each card also has a delete button which allows the user to delete specific shoes from their list.

Add a shoe: The add a shoe section opens up a form style page which allows the user to input information about a shoe and add it to their collection at a specific bin location. After heading back to the Shoes section and (reloading the page), a new shoe should appear in the list.

## Hats microservice
Models:
    - LocationVO holds the information for the physical storage location of the hats ie. closet name, section number and shelf number. This is including the import_href which is a unique identifer for each location.
    - The Hat model holds the information used for each specific hat ie. fabric, style, color and picute. There is a location Foreign Key using the LocationVO. If a location is removed the hats in that location are removed but they are not deleted from the database.

Integration with the Wardrobe API:
    - Using the poller, every 60 seconds it is set to check the system to make sure all application are running and that there are not real system errors. The hats poller is tied to locations within the Wardrobe-API.

Creating a location:
    {
	"closet_name": "daywear closet",
	"section_number": 6,
	"shelf_number": 1
}



## Shoes microservice
Models:
	BinVO: represents the physical storage bin within a wardrobe, containing specific shoes.
		‘Import_href’: unique identifier that helps refer to bins
		‘name’: field used to store bin name
	Shoe: the shoe model has all the relevant information about a particular pair of shoes.
        manufacturer: stores the name of the company that produced the shoe
        ‘name’: name of specific shoe
        ‘color’: the color of a the shoe
        ‘picture_url’: a link to a picture of the shoe.
        ‘Bin’: a foreign key relationship with the BinVO models, which makes a connection between the shoe and its physical location.

Integration with Wardrobe API:
	Using a poller, the service periodically requests data from the Wardrobe API, ensuring that the local “BinVO’ representations are accurate to the state of the wardrobe.


Instructions:
create Bin as follows:
{
	"closet_name": "first closet",
	"bin_number": "5",
	"bin_size": "10"

}

