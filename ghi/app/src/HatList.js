

function HatList(props) {
    const handleDelete = async (hatId) => {
        try {
          const response = await fetch(`http://localhost:8090/api/hats/${hatId}/`, { method: "DELETE" });
          if (!response.ok) {
            throw new Error("Invalid request");
          }

          props.onHatDeleted(hatId);
        } catch (error) {
          console.error("Error during DELETE request:", error);
        }
        window.location.reload();
      };


    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Location</th>
          </tr>
        </thead>
        <tbody>
        {props.hats.map(hat => {
            return (
              <tr key={hat.href}>
                <td>{ hat.fabric }</td>
                <td>{ hat.style }</td>
                <td>{ hat.color }</td>
                <td>{ hat.picture }</td>
                <td>{ hat.location }</td>
                <button onClick={() => handleDelete(hat.id)}>Delete</button>
              </tr>

            );
          })}
        </tbody>
      </table>
    );
  }

  export default HatList;

