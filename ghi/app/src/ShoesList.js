function ShoesList(props) {

  const handleDelete = async (shoeId) => {
    try {
      const response = await fetch(`http://localhost:8080${shoeId}`, { method: 'DELETE'});
      if (!response.ok) {
        throw new Error('Bad network response')
      }
      props.onShoeDeleted(shoeId)
    } catch (error) {
      console.error("cannot fetch:", error)
    }
    window.location.reload()
  }


 
      return (
        <div className="row mt-4">
          {props.shoes.map((shoe) => (
            <div key={shoe.picture_url} className="col-sm-6 col-md-4 col-lg-3">
              <div className="card mb-4 shadow-sm">
                <img src={shoe.picture_url} alt={shoe.name} className="bd-placeholder-img card-img-top" width="100%" height="260" />
                <div className="card-body">
                  <h5 className="card-title">{shoe.manufacturer}</h5>
                  <p className="card-text">{shoe.name}</p>
                  <div className="d-flex justify-content-between align-items-center">
                    <small className="text-muted">{shoe.color}</small>
                  </div>
                  <div className="d-flex justify-content-between">
                    <small className="text-muted">{shoe.bin.name}</small>
                    <button
                    type="button"
                    className="btn btn-sm btn-outline-danger"
                    onClick={() => handleDelete(shoe.href)}
                    >Delete</button>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      );
    }







    export default ShoesList;
